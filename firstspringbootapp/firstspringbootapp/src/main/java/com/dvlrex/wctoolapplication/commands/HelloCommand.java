package com.dvlrex.wctoolapplication.commands;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
public class HelloCommand {
    @ShellMethod(key= "hello", value = "This command will print Hello World!")
    public String hello(@ShellOption(defaultValue = "World") String arg) {
        return "Hello" + arg + "!";
    }
    @ShellMethod(key= "goodbye", value = "This command will print Goodbye!")
    public String goodBye() {
        return "Goodbye!";
    }
}
