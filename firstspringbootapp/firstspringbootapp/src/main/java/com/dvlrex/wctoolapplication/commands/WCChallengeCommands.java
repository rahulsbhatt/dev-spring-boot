package com.dvlrex.wctoolapplication.commands;

import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

@ShellComponent
@ShellCommandGroup("WC Tool Commands")
public class WCChallengeCommands {
    
    @ShellMethod(key="ccwc", value="Custom command with some options")
    public void printByte(@ShellOption(value = {"-c", "--count"}, help = "prints byte count") String filePath) throws IOException {
        try {
            long totalBytes = new java.io.File(filePath).length();
            System.out.printf("%s %s%n", totalBytes, filePath);
        }
        catch (Exception ex) {
            System.out.printf("An error occurred: %s" , ex.getMessage());
        }
    }
    @ShellMethod(key="ccwc", value="Custom command with some options")
    public void printNumOfLines(@ShellOption(value = {"-l", "--lines"}, help = "prints number of lines") String filePath) throws IOException {
        try(Scanner scanner = new Scanner(new FileReader(filePath))) {
            int lineCount = 0;
            while(scanner.hasNextLine()) {
                scanner.nextLine();
                lineCount++;
            }
            System.out.printf("%s %s", lineCount, filePath);
        }
        catch (IOException ex) {
            System.out.printf("An error occurred: %s" , ex.getMessage());
        }
    }

    @ShellMethod(key="ccwc", value="Custom command with some options")
    public void printNumOfWords(@ShellOption(value = {"-w", "--words"}, help = "prints number of words") String filePath) throws IOException {
        try(Scanner scanner = new Scanner(new FileReader(filePath))) {
            int numOfWordsInAFile = 0;
            while(scanner.hasNextLine()) {
                int numOfWordsPerLine= scanner.nextLine().split(" ").length;
                numOfWordsInAFile+= numOfWordsPerLine;
            }
            System.out.printf("%s %s", numOfWordsInAFile, filePath);
        }
        catch (IOException ex) {
            System.out.printf("An error occurred: %s" , ex.getMessage());
        }
    }

    @ShellMethod(key="ccwc", value="Custom command with some options")
    public void printNumOfChars(@ShellOption(value = {"-c", "--chars"}, help = "prints number of chars") String filePath) throws IOException {
        try(Scanner scanner = new Scanner(new FileReader(filePath))) {
            int numOfCharsInAFile = 0;
            while(scanner.hasNextLine()) {
                var line = scanner.nextLine();
                int numOfCharsPerLine= line.length();
                numOfCharsInAFile+= numOfCharsPerLine;
            }
            System.out.printf("%s %s", numOfCharsInAFile, filePath);
        }
        catch (IOException ex) {
            System.out.printf("An error occurred: %s" , ex.getMessage());
        }
    }
}
